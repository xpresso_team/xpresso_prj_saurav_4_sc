import json
import pika
import datetime
import time
## importing 'mysql.connector' as mysql for convenient
import mysql.connector as mysql

class SampleReceive():

    RABBITMQ_IP  = "rabbitmq_ip"
    RABBITMQ_PORT = "rabbitmq_port"
    TIME_LAG = "time_lag"
    MYSQL_IP = "mysql_host"
    MYSQL_PORT = "mysql_port"
    MYSQL_USER = "mysql_user"
    MYSQL_PWD = "mysql_password"
    MYSQL_DB = "mysql_database"

    def __init__(self):

        with open("./config/dev.json") as f:
            self.config = json.load(f)

        # Create a new channel with the next available channel number or pass
        # in a channel number to use
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(self.config[self.RABBITMQ_IP],
                                      self.config[self.RABBITMQ_PORT]))
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue='xpresspo')

        # self.db = mysql.connect(host=self.config[self.MYSQL_IP],
        #                         user=self.config[self.MYSQL_USER],
        #                         port = self.config[self.MYSQL_PORT],
        #                         passwd=self.config[self.MYSQL_PWD],
        #                         database=self.config[self.MYSQL_DB] )
        # self.cursor = self.db.cursor()

    def receive(self):

        connection = mysql.connect(host=self.config[self.MYSQL_IP],
                                user=self.config[self.MYSQL_USER],
                                port = self.config[self.MYSQL_PORT],
                                passwd=self.config[self.MYSQL_PWD],
                                database=self.config[self.MYSQL_DB] )
        cursor = connection.cursor()

        def callback(ch, method, properties, body):
            start = time.time()
            query = f'INSERT INTO data(message) values (\"{body.decode()}\");'
            cursor.execute(query)
            connection.commit()
            # print(" [x] Received %r" % body)
            print("Time taken for processing {} is {}".format(body,
                                                                      time.time()-start))
        self.channel.basic_consume(
            queue='xpresspo', on_message_callback=callback, auto_ack=True)
        print(' [*] Waiting for messages. To exit press CTRL+C')
        self.channel.start_consuming()

if __name__=="__main__":
    job  = SampleReceive()
    job.receive()